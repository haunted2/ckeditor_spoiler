jQuery(document).ready(function() {
  var spoilerTitle = jQuery('.spoiler-title');
  if (jQuery('.spoiler-toggle').hasClass('hide-icon')) {
    jQuery('.hide-icon').parent().parent().find('.spoiler-content').show();
  }
  if (jQuery('.spoiler-toggle').hasClass('show-icon')) {
    jQuery('.show-icon').parent().parent().find('.spoiler-content').hide();
  }
  spoilerTitle.click(function() {
    jQuery(this).parent().find('.spoiler-content').toggle();
    jQuery(this).find('.spoiler-toggle').toggleClass('show-icon').toggleClass('hide-icon');
  });
});
